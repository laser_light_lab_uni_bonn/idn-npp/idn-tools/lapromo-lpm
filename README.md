# LaProMo-LPM

The Laser Projection Module (short LaProMo, or even shorter LPM) is able to receive a network stream with very simply structured laser sample data and is able to ouput a laser data stream according to the IDN specification (ILDA Digital Network).

See the [IDN-NPP Documentation](https://gitlab.com/laser_light_lab_uni_bonn/idn-npp/Documentation) for details on the data structures of the laser point samples.

## Linear interpolation

The given laser sample data maybe extended by additional point samples using a very simple linear interpolation
to avoid damage to X-Y laser scanners if the distance between consecutive sample points is too large.

## LaProMo Configuration GUI

The LaProMo Configuration GUI can be used to make changes to the basic configuration of the laser image data, like geometric correction
of laser display size, offset, flip x and y orientation, and others. This is usually only relevant when using real laser projectors.
You can also change the parameters for linear interpolation (be careful with real laser!).

## Changing the IDN configuration of LaProMo

If you have real laser projector(s) with IDN consumer components available, you can modify the settings of LaProMo (Laser Projection
Module) to send the IDN stream to your real laser projector!

You can change some settings in the file idn.conf (with any text editor) to either manually set an IP address of your IDN consumer
component or you can change the settings to use IDN service discovery. In case of questions on this, please consult with
the Uni Bonn / Laser & Light Lab team!
